/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                   pong.js - A pong game example                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                  sourceforge.net/projects/retrogear/               *
 *                                                                    *
 *********************************************************************/ 

var screen = document.getElementById("screen");
var font = new Font("resources/gbfont.gif", 8, 8);

const PADDLE_WIDTH = 10;
const PADDLE_HEIGHT = 50;

var titleSpr;
var player_score = 0,
    cpu_score = 0,
    last_score = 1; //The last player to score, serves the ball (direction_x)

var Entities = new EntityList();
var ball=null, player1=null, player2=null;
var timer = new Timer(0, null);

function rectCollision(x1, y1, w1, h1, x2, y2, w2, h2)
{
    if (y1+h1 < y2) return 0;
    if (y1 > y2+h2) return 0;
    if (x1+w1 < x2) return 0;
    if (x1 > x2+w2) return 0;
    
    return 1;
}

function launchBall()
{
    ball.speed = 2.5;
}

function presetGame()
{
    player1.moveY(screen.clientHeight / 2 - (PADDLE_HEIGHT/2));
    player2.moveY(screen.clientHeight / 2 - (50/2));
    
    ball.x = screen.clientWidth / 2;
    ball.y = screen.clientHeight / 2;
    ball.direction_x = last_score;
    ball.direction_y = parseFloat( (Math.random() * (0.9 - 0.2))) * (Math.random() * (-1 - 1) + 1);

    ball.timer[0].set(2000, launchBall);
    ball.timer[0].start();
}

function updateBall()
{
    if(this.speed>=5)
    {
        this.speed=5;
    }

    this.x+=this.speed*this.direction_x;
    this.y+=(this.speed*this.direction_y);

    //Someone scores
    if(this.x+this.w > 240)
    { 
        player_score++;
        this.speed = 0;
        last_score = 1;
        presetGame();
    }
    
    if(this.x-1 < 0)
    { 
        cpu_score++;
        this.speed = 0;
        last_score = -1;
        presetGame();
    }

    if(this.y <= 0)
    {
        this.y=0;
        this.direction_y *= -1;
    }

    if(this.y+this.h >= screen.clientHeight)
    {
        this.y = screen.clientHeight-this.h;
        this.direction_y *= -1;
    }

    this.moveX(this.x);
    this.moveY(this.y);

    if(rectCollision(player1.x, player1.y, PADDLE_WIDTH, PADDLE_HEIGHT,
                     this.x+this.direction_x, this.y, this.w, this.h))
    {
        this.x = player1.x+player1.w+1;
        this.direction_x *= -1;
        this.speed *= 1.2;

        var paddleCenter = player1.y + (PADDLE_HEIGHT/2);
        this.direction_y = parseFloat( (Math.random() * (0.9 - 0.2))) * (ball.y<=paddleCenter? -1 : 1);
    }

    if(rectCollision(player2.x, player2.y, PADDLE_WIDTH, PADDLE_HEIGHT,
                     this.x+this.direction_x, this.y, this.w, this.h))
    {
        this.x = player2.x-this.w;
        this.direction_x *= -1;
        this.speed *= 1.2;

        var paddleCenter = player2.y + (PADDLE_HEIGHT/2);
        this.direction_y = parseFloat( (Math.random() * (0.9 - 0.2))) * (ball.y<=paddleCenter? -1 : 1);
    }
}

function updatePlayer()
{
    if(ball.x <= this.x+this.w)
    return;

    if( this.y < 0)
    {
        this.y = 0;
        return;
    }

    if( this.y+this.h > screen.clientHeight )
    {
        this.y = screen.clientHeight-this.h; 
    }

    if(this.y > 0 && Gamepad.isButtonPressed(Gamepad.button.UP))
    {
        this.moveY(this.y-this.speed);
    }

    if (this.y+PADDLE_HEIGHT < screen.clientHeight && Gamepad.isButtonPressed(Gamepad.button.DOWN))
    {
        this.moveY(this.y+this.speed);
    }
}

function updateCpu()
{
    //~ if(ball.x+ball.w < screen.clientWidth-80)
    //~ return;

    if(ball.direction_x>0 && ball.x < this.x-1)
    {
        if(ball.y > this.y && this.y+PADDLE_HEIGHT < screen.clientHeight)
        {
            this.moveY(this.y+this.speed);
        }
        
        if(ball.y < this.y && this.y >= 0)
        {
            this.moveY(this.y-this.speed);
        }
    }
    else
    {
        //Return to start position
        //~ if(this.y < this.ystart)
        //~ {
            //~ this.moveY(this.y+this.speed);
        //~ }
        //~ else if(this.y > this.ystart)
        //~ {
            //~ this.moveY(this.y-this.speed);
        //~ }
    }
}

function doTitle()
{
    titleSpr.draw();
    
    font.drawText("start", 80, screen.clientHeight-64, "PUSH START", "screen");

    font.drawText("title_credit2", 8, screen.clientHeight-24, "pix3lworkshop.altervista.org", "screen");
    font.drawText("title_credit1", 10, screen.clientHeight-16, "2022 Powered by RetroGearJS", "screen");

    if(Gamepad.isButtonPressed(Gamepad.button.START))
    {
        Game.setStatus(states.PREGAME);
    }
}

function doGame()
{
    if(player_score==9 || cpu_score==9)
    {
        timer.set(2000, Game.setStatus, (player_score>cpu_score)? states.END : states.LOST);
        timer.start();
        return;
    }
    
    font.drawText("score", 100, 8, player_score+" - "+cpu_score, "screen");

    var current = Entities.head;
    while (current)
    {
        current.update();
        current.draw();
        current = current.next;
    }
}

function init()
{
    Gamepad.init();

    titleSpr = new Sprite(0, screen.clientHeight / 2 - 64, 240, 64, 0, "games/pong/assets/title.gif");

    ball = new Entity('ball', 0, screen.clientWidth / 2 , screen.clientHeight / 2, 10, 10, 2, 0, updateBall, new Sprite(0, 0, 10, 10, 0, "games/pong/assets/ball.gif"));
    ball.direction_x=1;
    ball.direction_y=1;
    ball.addTimer(0, null);

    player1 = new Entity('player', 0, PADDLE_WIDTH*2, screen.clientHeight / 2 - (PADDLE_HEIGHT/2), PADDLE_WIDTH, PADDLE_HEIGHT, 2, 0, updatePlayer, new Sprite(0, 0, 10, 50, 0, "games/pong/assets/p1.gif"));
    player2 = new Entity('cpu', 0, screen.clientWidth - (PADDLE_WIDTH*3), screen.clientHeight / 2 - (50/2), PADDLE_WIDTH, PADDLE_HEIGHT, 2, 0, updateCpu, new Sprite(0, 0, 10, 50, 0, "games/pong/assets/p2.gif"));

    Entities.push(ball);
    Entities.push(player1);
    Entities.push(player2);

    Game.doTitle = doTitle;
    Game.doGame = doGame;

    var intervalID = setInterval(Game.mainLoop, 33);
}

