/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                  breakout.js - A breakout game example             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/ 

var screen = document.getElementById("screen");
var font = new Font("resources/gbfont.gif", 8, 8);

const PADDLE_WIDTH = 50;
const PADDLE_HEIGHT = 10;
const TILESIZE = 10;
const BRICK_W = 18;
const BRICK_H = 8;

var player_score = 0, hiscore=10000;
var lives = 3;
var nextBonus = 0;

//Entities
var ball=null, player1=null, extraLife=null;

var timer = new Timer(0, null);

//Graphics
var titleSpr = new Surface("Breakout", screen.clientWidth/4, screen.clientHeight / 2 - 40, 127, 48, "games/breakout/assets/title.gif");
var ballSpr = new Sprite(0, 0, 10, 10, 0, "games/breakout/assets/ball.gif"),
    genericSpr = new Sprite(0, 0, 10, 10, 0, "games/breakout/assets/sprites.gif"),
    explosionSpr = new Sprite(0, 0, 8, 8, 400, "games/breakout/assets/explosion.gif");

var heart = String.fromCharCode(8); //Heart icon

var ballImg = new Surface("ballImg", 0, 10, 10, 10, "games/breakout/assets/ball.gif");

var Entities = new EntityList(),
    Bricks = new EntityList();

var playfield = {
    init: false,
    x: 10, y:9, w: 18, h: 19
};

function rectCollision(x1, y1, w1, h1, x2, y2, w2, h2)
{
    if (y1+h1 < y2) return 0;
    if (y1 > y2+h2) return 0;
    if (x1+w1 < x2) return 0;
    if (x1 > x2+w2) return 0;
    
    return 1;
}

function presetGame()
{
    ball.sprite = ballSpr;
    ball.direction_x = parseFloat( Math.random() * (1 -1) + -1 );
    ball.direction_y = -1;
    ball.speed = 0;
    ball.x = player1.x+(player1.w/2)-(ball.w/2);
    ball.y = player1.y-TILESIZE;

    player1.moveX(76);
    player1.moveY(screen.clientHeight - PADDLE_HEIGHT*2);
}

function resetGame()
{
    lives = 3;
    nextBonus = 0;
    player_score = 0;
    playfield.init = false;
    presetGame();
}

function initPlayfield()
{
    if(!playfield.init)
    {
        var brickIdx=0;
        var brickSprIdx=0;
        var type=100;
        
        for(var row=0; row < 6; row++)
        {
            for(var col=0; col < 10; col++)
            {
                var _sprite = new Sprite(3+col*BRICK_W, row*BRICK_H, BRICK_W, BRICK_H, 0, "games/breakout/assets/bricks.gif");
                _sprite.surface.container.style.backgroundPosition = "-"+brickSprIdx+"px 0";

                var _brick = new Entity('brick'+brickIdx, type, TILESIZE+(col*BRICK_W), TILESIZE+(row*BRICK_H), BRICK_W, BRICK_H, 0, 0, null, _sprite);
                _brick.draw();

                Bricks.push(_brick);
                
                brickIdx++;
            }

            if(row%2)
            {
                brickSprIdx +=BRICK_W;
                type = type/2;
            }
        }

        var border = new Surface("border_ul-img", 0, 0, TILESIZE, TILESIZE, "games/breakout/assets/sprites.gif").draw();

        border = new Surface("border_ur-img", 30, 0, TILESIZE, TILESIZE, "games/breakout/assets/sprites.gif")
            .setPosition(TILESIZE+playfield.w*TILESIZE, 0)
            .clip(-30, 0)
            .draw();

        //Horizontal borders
        for(var i=0; i<playfield.w; i++)
        {
            var border = new Surface("border_h-"+i+"-img", 20, 0, TILESIZE, TILESIZE, "games/breakout/assets/sprites.gif")
                .setPosition(TILESIZE+(TILESIZE*i), 0)
                .clip(-20, 0)
                .draw();
        }

        //Vertical borders
        for(var i=1; i<playfield.h; i++)
        {
            var border = new Surface("border_vl-"+i+"-img", 10, 0, TILESIZE, TILESIZE, "games/breakout/assets/sprites.gif")
                .setPosition(0, i*TILESIZE)
                .clip(-10, 0)
                .draw();

            border = new Surface("border_vr-"+i+"-img", 40, 0, TILESIZE, TILESIZE, "games/breakout/assets/sprites.gif")
                .setPosition(TILESIZE+playfield.w*TILESIZE, i*TILESIZE)
                .clip(-40, 0)
                .draw();
        }
        
        playfield.init = true;
    }
}

function update1up()
{
    font.drawText("1up", this.x, this.y, heart);
    this.moveY(this.y+this.gravity);
    
    if(rectCollision(player1.x, player1.y, PADDLE_WIDTH, PADDLE_HEIGHT,
                     this.x, this.y+1, this.w, this.h))
    {
        lives++;
        Entities.pop(this);
    }
    
    if(this.y+this.h >= screen.clientHeight)
    {
        Entities.pop(this);
    }
}

function updateBall()
{
    if(this.speed>=5)
    {
        this.speed=5;
    }

    this.x += this.speed*this.direction_x;
    this.y += this.speed*this.direction_y;

    if(this.x < playfield.x)
    { 
        this.x=playfield.x;
        this.direction_x *= -1;
    }

    if(this.x+this.w > 190)
    { 
        this.x=190-this.w;
        this.direction_x *= -1;
    }

    if(this.y <= playfield.y)
    {
        this.y=playfield.y;
        this.direction_y *= -1;
    }

    //Ball lost
    if(this.y+this.h >= screen.clientHeight)
    {
        this.speed = 0;

        this.sprite = explosionSpr;
        this.sprite.setAnimationSequence([0,1,2,-1]);
        this.sprite.animate();

        if(this.sprite.hasAnimationEnded())
        {
            presetGame();
            lives--;
        }

        return;
    }

    this.moveX(this.x);
    this.moveY(this.y);

    if(rectCollision(player1.x, player1.y, PADDLE_WIDTH, PADDLE_HEIGHT,
                     this.x+this.direction_x, this.y, this.w, this.h))
    {
        this.direction_y *= -1;
        this.speed *= 1.2;

        var paddleCenter = player1.x + (PADDLE_HEIGHT/2);
        this.direction_x = parseFloat( (Math.random() * (0.9 - 0.2))) * (ball.x<=paddleCenter? -1 : 1);
    }
    
    var current = Bricks.head;
    while (current!=null)
    {
        if(rectCollision(current.x, current.y, current.w, current.h,
                         this.x+this.direction_x, this.y+this.direction_y, this.w-2, this.h-2))
        {
            this.direction_y *= -1;
            this.speed *= 1.2;
            
            var brickCenter = current.x + current.w / 2;
            
            player_score+=current.type;

            nextBonus += Math.floor(Math.random() * 5);
            if(!Entities.exist('1up'))
            {
                if(nextBonus >= 50) //If the sum of random numbers are greater than 50, we release an extra life
                {
                    var up = new Entity('1up', 0, brickCenter, current.y+current.h+1, TILESIZE, TILESIZE, 0, 1, update1up);
                    up.draw();
                    Entities.push(up);

                    nextBonus = 0;
                }
            }
            
            Bricks.pop(current);
            current=null;
            
            if(player_score>hiscore)
            {
                hiscore = player_score;
            }
            
            break;
        }
        
        current = current.next;
    }
}

function updatePlayer()
{
    if( this.x < 10)
    {
        this.x = 10;
        return;
    }

    if(ball.speed==0)
    {
        ball.x = this.x+(this.w/2)-(ball.w/2);

        if(ball.timer[0].waitDelay(2000) || Gamepad.isButtonPressed(Gamepad.button.A))
        {
            ball.speed = 2;
        }
    }

    if( this.x+this.w > screen.clientHeight )
    {
        this.x = screen.clientHeight-this.w; 
    }

    if(this.y > ball.y+ball.h-1 )
    {
        if(Gamepad.isButtonPressed(Gamepad.button.LEFT))
        {
            if(this.x > playfield.x)
            {
                this.moveX(this.x-this.speed);
            }
        }

        if (this.x+PADDLE_WIDTH < (playfield.w*TILESIZE)+TILESIZE && Gamepad.isButtonPressed(Gamepad.button.RIGHT))
        {
            this.moveX(this.x+this.speed);
        }
    }
}

function doTitle()
{
    if(!screen.innerHTML.trim())
    {
        //Fake blocks
        var brickSprIdx=0;
        for(var row=0; row < 6; row++)
        {
            for(var col=0; col < 13; col++)
            {
                var _brick = new Sprite(3+col*BRICK_W, row*BRICK_H, BRICK_W, BRICK_H, 0, "games/breakout/assets/bricks.gif");
                _brick.surface.container.style.backgroundPosition = "-"+brickSprIdx+"px 0";
                _brick.draw();
            }

            if(row%2)
            {
                brickSprIdx +=BRICK_W;
            }
        }
    }
    
    presetGame();
    titleSpr.draw();
    
    font.drawText("start", 80, screen.clientHeight-64, "PUSH START");

    font.drawText("title_credit2", 8, screen.clientHeight-24, "pix3lworkshop.altervista.org");
    font.drawText("title_credit1", 10, screen.clientHeight-16, "2022 Powered by RetroGearJS");

    if(!Gamepad.isButtonHeld(Gamepad.button.START) && Gamepad.isButtonPressed(Gamepad.button.START))
    {
        Game.setStatus(states.PREGAME);
    }
}

function doGame()
{
    if(!playfield.init)
    {
        initPlayfield();
    }
    
    font.drawText("txt_hiscore", (playfield.w*TILESIZE)+TILESIZE*2, 24, "TOP");
    font.drawText("hiscore", (playfield.w*TILESIZE)+TILESIZE*2-2, 32, hiscore);

    font.drawText("txt_score", (playfield.w*TILESIZE)+TILESIZE*2, 48, "SCORE");
    font.drawText("score", (playfield.w*TILESIZE)+TILESIZE*2-1, 56, player_score);

    font.drawText("txt_stage", (playfield.w*TILESIZE)+TILESIZE*2, 88, "STAGE");
    font.drawText("stage", (playfield.w*TILESIZE)+TILESIZE*2, 102, "01");

    var x = (playfield.w*TILESIZE)+TILESIZE*2;

    ballImg.setPosition(x+2, 135);
    ballImg.draw();

    font.drawText("lives", x+TILESIZE+4, 136, "x"+lives);

    if(lives<=0)
    {
        Game.timer.set(1000, Game.status=states.GAMEOVER);
        Game.timer.start();
        return;
    }

    if(Bricks.head==null)
    {
        ball.sprite = explosionSpr;
        ball.sprite.setAnimationSequence([0,1,2]);
        ball.sprite.animate();
        ball.draw();

        if(ball.sprite.hasAnimationEnded())
        {
            Game.setStatus(states.END);
            return;
        }

        return;
    }
    
    var current = Entities.head;
    while (current)
    {
        if(current.update!=null)
            current.update();

        current.draw();
        current = current.next;
    }
}

function doGameOver()
{
    font.drawText("game_over", 65, screen.clientHeight >> 1, "GAME OVER");
    font.drawText("restart", 33, (screen.clientHeight >> 1) + _defaultFont.getCharHeight(), "Push A to restart!");

    if(Gamepad.isButtonPressed(Gamepad.button.A))
    {
        resetGame();
        Game.setStatus(states.TITLE);
    }

    //~ Game.timer.set(3000, Game.setStatus, states.TITLE);
    //~ Game.timer.start();
}

function init()
{
    Gamepad.init();

    player1 = new Entity('player', 0, 76, screen.clientHeight - PADDLE_HEIGHT*2, PADDLE_WIDTH, PADDLE_HEIGHT, 2, 0, updatePlayer, 
                        new Sprite(0, 0, PADDLE_WIDTH, PADDLE_HEIGHT, 0, "games/breakout/assets/p1.gif"));

    ball = new Entity('ball', 0, 96 , player1.y-TILESIZE, TILESIZE, TILESIZE, 0, 0, updateBall, ballSpr);
    ball.addTimer(0, null);
    
    Entities.push(ball);
    Entities.push(player1);

    Game.doTitle = doTitle;
    Game.doGame = doGame;
    Game.doGameOver = doGameOver;

    var intervalID = setInterval(Game.mainLoop, 33);
}

