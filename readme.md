# RetroGearJS
## A silly DHTML game engine inspired by RetroGear

Released under **GPL license**, **RetroGearJS** is a pet project inspired by **RetroGear**.
It's goal is to provide a really simple and stupid DHTML game engine just for fun, compatible at least with Firefox 1.0 and above.

It provides:

 - Game states management (screen title, pre-game, game, game over)  
 - Game entities managment
 - A simple DOM elements handling for graphical purpose (No canvas!)
 - A DOM based bitmap font
 - Touch screen support for mobile (For more recent browsers out there...)
 - Centrilized javascript input handling and events (touch also) across PCs and mobiles!
 - A really cool pocket console interface (Batteries included!)

Checkout the official repository at [https://gitlab.com/Pix3l/retrogearjs](https://gitlab.com/Pix3l/retrogearjs)!

## Games

Play it in your browser! (Just click the image)

![Pong](images/shots/pong.png)
![Breakout](images/shots/breakout.png)

## Examples

Poke around with the code through the examples!

 - [FPS](examples/fps.htm)
 - [Input](examples/input.htm)
 - [Entity](examples/entity.htm)
 - [Sprites](examples/sprites.htm)
 - [Font](examples/font.htm)
 - [Menu](examples/menu.htm)
 - [Typewriter effect](examples/typewriter.htm)
