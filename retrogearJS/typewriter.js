/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *              typewriter.js - Virtual typewriter for text           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var curr_typewriter = null;

var Typewriter_flags = {
    INACTIVE         : 1 << 0,  // 000001  1
    ACTIVE     : 1 << 1,  // 000010  2
    PAUSED    : 1 << 2,  // 000100  4
    STOPPED     : 1 << 3,  // 000100
    //~ flag     = 8     // 001000  8
};

var Typewriter = function(id, text, x=0, y=0, width="100%")
{
    this.id = "typewriter-"+id;
    this.flags = 0;
    this.x = x;
    this.y = y;

    this.max_width = 0;
    this.curr_index = 0;
    this.text = text;
    this.delay = 1;

    /**
     * The default container is a modified font container.
     * The suffix "-text" is autocatching the newly created font container
     **/
    this.container = font.drawText(this.id+"-text", this.x, this.y, "");
    this.container.style.zIndex = 10;
    this.container.style.width = 222 + "px";
    this.container.style.height = 44 + "px";
    this.container.style.border= "1px solid black";
    this.container.style.padding= "8px";

    //Use default callbacks
    this.update = this.update;
    this.draw = this.draw;
}

Typewriter.prototype.update = function()
{
    if(this.text.lenght<=0)
    {
        return;
    }

    //Check if the text is finished
    if( this.isTypewriterEnd(typewriter) )
    {
        this.flag_status = Typewriter_flags.PAUSED;
    }
    
    //Handle the pause (Page end, forced pause, other...)
    if(this.flag_status == Typewriter_flags.PAUSED)
    {
        if(Gamepad.isButtonPressed(Gamepad.button.A))
        {
            //Check if the text is finished
            if( this.isTypewriterEnd(typewriter) )
            {
                /**
                 * instead of reset the entire object, we simply switch
                 * back to the start of the text
                 **/
                this.resetTypewriter();
                return;
            }
            
            if( this.text.charAt(this.curr_index) =='\f' )
            {
                this.text += this.curr_index+1;
                this.curr_index = 0;
                
                Gamepad.setButtonReleased(Gamepad.button.A);
            }
            else
            {
                this.text = '';
                return;
            }
            
            this.flags=Typewriter_flags.ACTIVE;
        }
        
        return;
    }
	
    //Decrease the delay...
    this.delay--;

    //Speed up
    if(Gamepad.isButtonPressed(Gamepad.button.A))
    {
        this.delay=0;
    }

    if(this.delay == 0)
    {
        this.curr_index++;
        this.delay=7;    //Reset delay
        
        //Formfeed character handle page change
        if( this.text.charAt(this.curr_index)=='\f' || this.text.charAt(this.curr_index) == null )
        {
            //Set a pause for next page
            this.flags=Typewriter_flags.PAUSED;
        }
    }

    //End of text
    if( this.isTypewriterEnd(typewriter) )
    {
        this.flags=Typewriter_flags.PAUSED;
        Gamepad.setButtonReleased(Gamepad.button.A);
    }
}

Typewriter.prototype.isTypewriterEnd = function()
{
    if( this.text.lenght <=0 || this.text.charAt(this.curr_index) == null )
    {
        return 1;
    }
    return 0;
}

Typewriter.prototype.reset = function()
{
    this.flag_status = Typewriter_flags.INACTIVE;
    this.curr_index = 0;
    this.text = '';
    this.delay = 1;  //This make the typewriter be able to start!

    curr_typewriter = this;
}

Typewriter.prototype.draw = function()
{
    //Make sure we have text to draw
    if(this.text.lenght<=0)
    {
        return;
    }

    var substring = this.text.substring(0, this.curr_index);
    font.drawText(this.id+"-text", this.x, this.y, substring);

    //If there are other pages, we draw the flashing arrow
    //~ if(this.flag_status == Typewriter_flags.PAUSED)
    //~ {
        //~ if(get_time_elapsed(5)%2)
        //~ {
            //~ return;
        //~ }
        //~ drawChar(this.x+(this.max_width-FONT_W), this.y+FONT_H*7, 31, white, 0);
    //~ }
}
