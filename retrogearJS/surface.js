/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                     surface.js - Drawing context                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var Surface = function(id, x, y, w, h, file)
{
    //If image already exists in DOM, we skip it
    if(document.getElementById(id))
    {
        return;
    }

    (new Image()).src = file;    //Preload
    
    this.w = w;
    this.h = h;
    
    this.container = document.createElement('div');
    this.container.id = id;
    this.container.style.position = "absolute";
    this.container.style.background = "url('"+file+"')";
    this.container.style.backgroundRepeat = "no-repeat";

    this.container.style.left = x + "px";
    this.container.style.top = y + "px";
    this.container.style.width = w + ((typeof w === "number")?"px": "");
    this.container.style.height = h + ((typeof w === "number")?"px": "");
    this.container.style.backgroundPosition = "0 0";
    this.container.style.zIndex = "1";
}

Surface.prototype.setPosition = function(x, y)
{
    this.container.style.left = x + 'px';
    this.container.style.top  = y + 'px';
    
    return this;
}

Surface.prototype.clip = function(x, y)
{
    this.container.style.backgroundPosition = parseInt(x)+"px "+parseInt(y)+"px";
    return this;
}

Surface.prototype.setDepth = function(zIndex)
{
    this.container.style.zIndex = zIndex;
    return this;
}

Surface.prototype.draw = function()
{
    //If image already exists in DOM, we skip it
    if(this.container.parentNode==null)
    {
        if(!document.getElementById(this.container.id))
        {
            document.getElementById("screen").appendChild(this.container);
        }
        
        this.container = document.getElementById(this.container.id);
        
        this.container.style.left = this.x + 'px';
        this.container.style.top  = this.y + 'px';
    }
    
    return this;
};
