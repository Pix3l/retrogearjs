/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 * 						input.js - Input handling					  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *					   pix3lworkshop.altervista.org                   *
 *				   https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var keyboardMap=["","","","CANCEL","","","HELP","","BACK_SPACE","TAB","","","CLEAR","ENTER","ENTER_SPECIAL","","SHIFT","CONTROL","ALT","PAUSE","CAPS_LOCK","KANA","EISU","JUNJA","FINAL","HANJA","","ESCAPE","CONVERT","NONCONVERT","ACCEPT","MODECHANGE","SPACE","PAGE_UP","PAGE_DOWN","END","HOME","LEFT","UP","RIGHT","DOWN","SELECT","PRINT","EXECUTE","PRINTSCREEN","INSERT","DELETE","","0","1","2","3","4","5","6","7","8","9","COLON","SEMICOLON","LESS_THAN","EQUALS","GREATER_THAN","QUESTION_MARK","AT","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","OS_KEY","","CONTEXT_MENU","","SLEEP","NUMPAD0","NUMPAD1","NUMPAD2","NUMPAD3","NUMPAD4","NUMPAD5","NUMPAD6","NUMPAD7","NUMPAD8","NUMPAD9","MULTIPLY","ADD","SEPARATOR","SUBTRACT","DECIMAL","DIVIDE","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14","F15","F16","F17","F18","F19","F20","F21","F22","F23","F24","","","","","","","","","NUM_LOCK","SCROLL_LOCK","WIN_OEM_FJ_JISHO","WIN_OEM_FJ_MASSHOU","WIN_OEM_FJ_TOUROKU","WIN_OEM_FJ_LOYA","WIN_OEM_FJ_ROYA","","","","","","","","","","CIRCUMFLEX","EXCLAMATION","DOUBLE_QUOTE","HASH","DOLLAR","PERCENT","AMPERSAND","UNDERSCORE","OPEN_PAREN","CLOSE_PAREN","ASTERISK","PLUS","PIPE","HYPHEN_MINUS","OPEN_CURLY_BRACKET","CLOSE_CURLY_BRACKET","TILDE","","","","","VOLUME_MUTE","VOLUME_DOWN","VOLUME_UP","","","SEMICOLON","EQUALS","COMMA","MINUS","PERIOD","SLASH","BACK_QUOTE","","","","","","","","","","","","","","","","","","","","","","","","","","","OPEN_BRACKET","BACK_SLASH","CLOSE_BRACKET","QUOTE","","META","ALTGR","","WIN_ICO_HELP","WIN_ICO_00","","WIN_ICO_CLEAR","","","WIN_OEM_RESET","WIN_OEM_JUMP","WIN_OEM_PA1","WIN_OEM_PA2","WIN_OEM_PA3","WIN_OEM_WSCTRL","WIN_OEM_CUSEL","WIN_OEM_ATTN","WIN_OEM_FINISH","WIN_OEM_COPY","WIN_OEM_AUTO","WIN_OEM_ENLW","WIN_OEM_BACKTAB","ATTN","CRSEL","EXSEL","EREOF","PLAY","ZOOM","","PA1","WIN_OEM_CLEAR",""];

var Gamepad = {

    button : {  LEFT:37,
                UP:38,
                RIGHT:39, 
                DOWN:40,
                A:88,
                B:90, 
                START:13,
                SELECT:16
            },
    
    flags: { button_released:0,
             button_pressed:1,
             button_held:2,
             button_wait_release:3
            },
                
    state: new Array(),
    buttons: new Array(),
    time: new Array(),

    init: function()
    {
        //~ document.addEventListener(Event.KEYDOWN | Event.KEYUP, this.handleKeyboard);
        document.onkeydown = function(event){Gamepad.handleKeyboard(event);};
        document.onkeyup = function(event){Gamepad.handleKeyboard(event);};

        this.buttons.push(this.button.A, 
                          this.button.B,
                          this.button.START,
                          this.button.SELECT,
                          this.button.LEFT,
                          this.button.RIGHT,
                          this.button.DOWN,
                          this.button.UP
                        );

        this.state.push(0,0,0,0,0,0,0,0);
        this.time.push(0,0,0,0,0,0,0,0);
    },

    searchButton: function(keyCode)
    {
        for(var i=0; i<this.buttons.length; i++)
        {
            if( this.buttons[i] == keyCode )
            {
                return i;
            }
        }
        
        return null;
    },
    
    handleKeyboard: function(ev) 
    {
        for(var i=0; i<this.buttons.length; i++)
        {
            if( this.buttons[i] == ev.keyCode )
            {
                if(ev.type == "keydown" )
                {
                    /**
                     * Stupid hack borrowed from
                     * https://stackoverflow.com/questions/6087959/prevent-javascript-keydown-event-from-being-handled-multiple-times-while-held-do
                     **/
                    if( this.state[i] != this.flags.button_pressed && this.state[i] != this.flags.button_held)
                        this.setButtonPressed(ev.keyCode);
                    continue;
                }
                
                if(ev.type == "keyup" )
                {
                    this.setButtonReleased(ev.keyCode);
                    continue;
                }
            }
        }
    },
    
    isButtonPressed: function(keyCode, single)
    {
        if(this.state===null)
        {
            //~ console.log("[Gamepad] vGamepad not initialized!");
            return;
        }
        
        var index = this.searchButton(keyCode);

        var state = this.state[index];
        if( state != this.flags.button_wait_release && state != this.flags.button_released)
        {
            if((+new Date) > this.time[index]+10)
            {
                //~ console.log("[Gamepad] Button "+keyboardMap[keyCode]+" is being held");

                this.state[index] = this.flags.button_held;
                this.time[index] = 1.1;
                return true;
            }

            //~ console.log("[Gamepad] Button "+keyboardMap[keyCode]+" is pressed");
            this.state[index] = this.flags.button_pressed;
            this.time[index]+=0.6;
            
            return true;
        }
        
        return false;
    },

    isButtonHeld: function(keyCode)
    {
        var index = this.searchButton(keyCode);
        var key = String.fromCharCode(keyCode);
        
        if( this.state[index] == this.flags.button_held )
        {
            //console.log("[Gamepad] Button "+keyboardMap[keyCode]+" is being held");
            return true;
        }

        return false;
    },

    setButtonPressed: function (keyCode)
    {
        var index = this.searchButton(keyCode);

        if( this.state[index] == this.flags.button_pressed || this.state[index] == this.flags.button_held)
        {
            //~ console.log("[Gamepad] Button "+keyboardMap[keyCode]+" is being held");
            this.state[index] = this.flags.button_held;
            return;
        }

        //~ console.log("[Gamepad] Button "+keyboardMap[keyCode]+" is pressed");
        this.state[index] = this.flags.button_pressed;
        this.time[index] = +new Date;
    },

    /**
     * Javascript input events seems to be not immediate,
     * we need a fast way to force the "held" state
     **/
    setButtonHeld: function (keyCode)
    {
        var index = this.searchButton(keyCode);
        this.state[index] = this.flags.button_held;
    },
    
    setButtonReleased: function (keyCode)
    {
        var index = this.searchButton(keyCode);
        
        //console.log("[Gamepad] Button "+keyboardMap[keyCode]+" is released");
        this.state[index] = this.flags.button_released;
        this.time[index] = 0;
    }
};
