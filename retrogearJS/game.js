/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                      game.js - Game handling function              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/ 

var states = {GAMEOVER: 0, TITLE: 1, PREGAME: 2, GAME: 3, LOST: 4, END: 5, SPLASH: 6, IDLE: 7};

var Game = {

    timer: new Timer(3000, null),
    status: states.TITLE,

    setStatus: function(status)
    {
        //~ //Reset entity list
        //~ if (typeof EntityList !== 'undefined')
        //~ {
            //~ EntityList = new Array();
        //~ }
        
        document.getElementById("screen").innerHTML = ""; 
        Game.status = status;
        Game.timer.stop();
    },
    
    doTitle: function()
    {
        font.drawText("start", 10, 10, 80, screen.height-64, "PUSH START", "screen");

        font.drawText("title_credit2", 8, screen.height-24, "pix3lworkshop.altervista.org", "screen");
        font.drawText("title_credit1", 16, screen.height-16, "2019 - 2022 Powered by RetroGearJS", "screen");

        if(Gamepad.isButtonPressed(Gamepad.button.START))
        {
            Game.timer.set(3000, Game.setStatus, states.PREGAME);
            Game.timer.start();
        }
    },

    doPreGame: function()
    {
        font.drawText("pregame_stage", 82, screen.clientHeight/2, "GET READY!", "screen");

        Game.timer.set(2000, Game.setStatus, states.GAME);
        Game.timer.start();
    },

    doGame: function()
    {
        for (var index = 0; index < EntityList.length; index++)
        {
            EntityList[index].update();
            EntityList[index].draw();
        }
    },

    doWin: function()
    {
        font.drawText("win", 82, screen.clientHeight/2, "YOU WIN!", "screen");

        Game.timer.set(3000, Game.setStatus, states.TITLE);
        Game.timer.start();
    },

    doGameOver: function()
    {
        font.drawText("game_over", 85, screen.clientHeight/2, "GAME OVER", "screen");

        Game.timer.set(3000, Game.setStatus, states.TITLE);
        Game.timer.start();
    },

    mainLoop: function()
    {
        switch(Game.status)
        {
            //~ case states.SPLASH:
                //~ this.splashScreen();
            //~ break;
            case states.IDLE:
                //Do nothing
            break;
            case states.TITLE:
                Game.doTitle();
            break;
            case states.PREGAME:
                Game.doPreGame();
            break;
            case states.GAME:
                Game.doGame();
            break;
            case states.LOST:
                Game.doGameOver();
            break;
            case states.END:
                Game.doWin();
            break;
            case states.GAMEOVER:
                Game.doGameOver();
            break;
        }
    },
}

