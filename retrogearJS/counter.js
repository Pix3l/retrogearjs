/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                 counter.js - Graphical counter object              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *						Written by Cerullo Davide					  *
 *					   pix3lworkshop.altervista.org					  *
 *				   https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var counter = function(id, x, y, start=0, step=1, max=999)
{
    this.id = id;
    this.value = start;
    this.start = start;
    this.step = step;
    this.max = max;

    var container = document.createElement('div');
    container.id = "counter-"+id;
    container.name = id;
    container.style.position = "absolute";
    container.style.left = x + "px";
    container.style.top = y + "px";

    var value = this.value.toString();
    for(var i=0; i<value.length; i++)
    {
        
    }

    this.print = function (id, x, y, text)
    {
        var screen = document.getElementById("screen");
        if(!screen)
        {
            return;
        }

        var value = this.value.toString();

        var xPixOffset, yPixOffset;
        for (var i=0; i < text.length; i++)
        {
            if(text[i]=="\n")
            {
                var nl = document.createElement('br');
                string_container.appendChild(nl);
                continue;
            }
            
            xPixOffset = parseInt( ( Math.floor(text.charCodeAt(i) % 32)) * this.char_w );
            yPixOffset = parseInt( ( Math.floor(text.charCodeAt(i) / 32)) * this.char_h );

            var character = document.createElement('div');
            character.style.position = "relative";
            character.style.width = this.char_w + "px";
            character.style.height = this.char_h + "px";
            character.style.float = "left";

            character.style.background = "url('"+this.font+"')";
            character.style.backgroundRepeat = "no-repeat";
            character.style.backgroundPosition = "-"+xPixOffset+"px -"+yPixOffset+"px";

            string_container.appendChild(character);
        }

        screen.appendChild(string_container);
    };
}

function counter(id, x, y, padding, value)
{
    this.id = id;
    this.x = x;
    this.y = x;
    this.padding = padding;
    this.container = createDiv(this.id, x, y);

    this.setCounter = function(value)
    {
        this.value = parseInt(value);
        this.updateCounter();
    }

    this.incrementCounter = function(value)
    {
        this.value += parseInt(value);
        this.updateCounter();
    }

    this.updateCounter = function()
    {
        var str = '' + this.value;
        while (str.length < padding) {
            str = '0' + str;
        }

        var buffer = drawString(id, x, y, str);

        this.container.innerHTML = "";
        this.container.innerHTML = buffer;
    }

    if(value)
    {
        this.value = parseInt(value);
        this.updateCounter();
    }
    else
    {
        this.value = 0;
        this.updateCounter();
    }
}
