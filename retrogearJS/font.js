/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *             font.js - Image font handling functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var Font = function(file, charWidth, charHeight)
{
    (new Image()).src = file;    //Preload

    this.file = file;
    this.char_w = charWidth;
    this.char_h = charHeight;
}

Font.prototype.getCharWidth = function()
{
    return parseInt(this.char_w);
}

Font.prototype.getCharHeight = function()
{
    return parseInt(this.char_h);
}

Font.prototype.renderCharacter = function(chr)
{
    var xPixOffset = parseInt( ( Math.floor(chr.charCodeAt(0) % 32)) * this.getCharWidth() );
    var yPixOffset = parseInt( ( Math.floor(chr.charCodeAt(0) / 32)) * this.getCharHeight() );
    
    var character = document.createElement('div');
    character.style.position = "relative";
    character.style.width = this.getCharWidth() + "px";
    character.style.height = this.getCharHeight() + "px";
    character.style.float = "left";

    character.style.background = "url('"+this.file+"')";
    character.style.backgroundRepeat = "no-repeat";
    character.style.backgroundPosition = "-"+xPixOffset+"px -"+yPixOffset+"px";

    return character;
}

Font.prototype.renderText = function (id,text)
{
    var container = document.getElementById(id);
    if(container)
    {
        var real_text = container.firstChild.innerHTML;
        if(real_text!=undefined && real_text===text)
        {
            //~ console.log("[Font] Text hasn't changed, return the current container");
            return container;
        }
    }
    else
    {
        container = document.createElement('div');
        container.id = id;
        container.style.position = "absolute";
        container.innerHTML = document.createElement('span');
        container.style.fontSize = 0;   //Hide the original text
    }

    container.innerHTML = "<span>"+text+"</span>";     //Insert the original text for comparing

    text = text+""; //Convert everything to text

    //Convert every character to an image equivalent
    for (var i=0; i < text.length; i++)
    {
        if(text[i]=="\n")
        {
            var nl = document.createElement('div');
            //~ nl.style.clear = "both";
            nl.style.position = "relative";
            nl.style.height = this.getCharHeight()+"px";
            container.appendChild(nl);
            continue;
        }

        var character = this.renderCharacter(text[i]);
        container.appendChild(character);
    }
    
    return container;
}

Font.prototype.drawText = function(id, x, y, text, target)
{
    var container = this.renderText(id, text);
    
    //Update container text position according to parameters
    container.style.left = x + "px";
    container.style.top = y + "px";

    target = document.getElementById( (!target)? "screen" : target );

    if(container.parentNode==null)
    {
        target.appendChild(container);
    }
    
    return container;
}

var _defaultFont = new Font("resources/gbfont.gif", 8, 8);
