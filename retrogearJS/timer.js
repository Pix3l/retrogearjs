/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 * 					     timer.js - Generic timer                     *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *						Written by Cerullo Davide					  *
 *					   pix3lworkshop.altervista.org					  *
 *				   https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/ 

var Timer = function (interval, callback, params=null, repeat=false)
{
    var self = this;
    self.tid = null;
    self.last = 0;
    self.delay = 0;
    self._interval = interval;
    self._callback = callback;
    self._params = params;
    
    self.update = function()
    {
        //
        // Time difference accumulation
        //
        var curr = +new Date;

        if(curr - self.last >= self._interval)
        {
            if(self._callback)
            {
                self._callback(self._params);
            }
            
            self.delay %= self._interval;

            if(!self.repeat)
            {
                self.stop();
            }
        }
    }

    self.set = function(interval, callback, params=null)
    {
        self._interval = interval;
        self._callback = callback;
        self._params = params;
    }

    self.start = function()
    {
        if(self.tid!=null)
        {
            return;
        }

        self.last = +new Date;   
        self.delay = 0;
        self.tid = setInterval(self.update, 33);
    };

    self.stop = function()
    {
        clearInterval(self.tid);
        self.tid = null;
    };

    self.waitDelay = function(interval)
    {
        self.set(interval, null, null);
        self.start();

        if(self.tid == null)
        {
            return true;
        }

        return false;
    };
}
