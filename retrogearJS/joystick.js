/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 * 					   joystick.js - Virtual joystick                 *
 *                                                                    *
 *          This code is inspired by the work of Lee Stemkoski        *
 *            https://github.com/stemkoski/HTML-Joysticks             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *						Written by Cerullo Davide					  *
 *					   pix3lworkshop.altervista.org					  *
 *				   https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

/***
 * Usage instructions
 *
 * Add the joystick structure to your page like this:
 * 
 *  <div id="stick1">
 *      <div id="trackball" style="border-radius: 32px; width: 48px; height: 48px;"></div>
 *  </div>
 *
 * It's important to set width and height to the the trackball!
 *
 * Enable it via javascript:
 * 
 * <script type="text/JavaScript">
 *     var joystick1 = new Joystick("stick1", 46, 8, true);
 * </script>
 * 
 ***/

/**
 * A virtual "trackball" joystick object
 *
 *  @param id
 *      ID of HTML element (representing joystick) that will be dragged
 *
 *  @param maxDistance
 *      Maximum amount joystick can move in any direction
 *
 *  @param deadzone
 *      Joystick must move at least this amount from origin to register value change
 **/
var Joystick = function(id, maxDistance, deadzone, debug=false)
{
    var target = document.getElementById(id);

    // location from which drag begins, used to calculate offsets
    this.startPoint = { x: 0, y: 0 };

    // track touch identifier in case multiple joysticks present
    this.touchId = null;

    this.active = false;
    this.value = { x: 0, y: 0 };

    var self = this;

    if(debug)
    {
        target.parentNode.style.border = "1px solid #ff0000";
        target.parentNode.style.borderRadius = "64px";

        //~ if (target.hasChildNodes())
        //~ {
            //~ var children = target.childNodes;

            //~ for (var i = 0; i < children.length; i++)
            //~ {
                //~ if(children[i].tagName=="DIV")
                //~ {
                    //~ children[i].style.background = "#ff0000";
                    //~ break;
                //~ }
            //~ }
        //~ }
    }

    function resetGamepad()
    {
        var directions = [Gamepad.button.LEFT, Gamepad.button.RIGHT, Gamepad.button.UP, Gamepad.button.DOWN];
        for(var i=0; i<directions.length; i++)
        {
            Gamepad.setButtonReleased(directions[i]);
        }
    }

    function translateButton(angle)
    {
        var degree = parseInt(angle * 180 / Math.PI);
        //~ console.log(degree);
        
        resetGamepad();

        if(degree >= -115 && degree <= -50)
        {
            Gamepad.setButtonPressed(Gamepad.button.UP);
            console.log("UP");
        }

        if(degree >= -50 && degree <= -25)
        {
            Gamepad.setButtonPressed(Gamepad.button.UP);
            Gamepad.setButtonPressed(Gamepad.button.RIGHT);
            console.log("UP-RIGHT");
        }

        if(degree >= -25 && degree <= 30)
        {
            Gamepad.setButtonPressed(Gamepad.button.RIGHT);
            console.log("RIGHT");
        }

        if(degree >= 20 && degree <= 50)
        {
            Gamepad.setButtonPressed(Gamepad.button.DOWN);
            console.log("RIGHT-DOWN");
        }

        if(degree >= 50 && degree <= 140)
        {
            Gamepad.setButtonPressed(Gamepad.button.DOWN);
            console.log("DOWN");
        }

        if(degree >= 140 && degree <= 150)
        {
            Gamepad.setButtonPressed(Gamepad.button.LEFT);
            Gamepad.setButtonPressed(Gamepad.button.DOWN);
            console.log("LEFT-DOWN");
        }

        if(degree >= 150 && degree <= 180 || degree >=-179 && degree <=-150)
        {
            Gamepad.setButtonPressed(Gamepad.button.LEFT);
            console.log("LEFT");
        }

        if(degree >= -150 && degree <= -115)
        {
            Gamepad.setButtonPressed(Gamepad.button.LEFT);
            Gamepad.setButtonPressed(Gamepad.button.UP);
            console.log("LEFT-UP");
        }
    }

    function handle(ev)
    {
        if (typeof Gamepad == "undefined")
        {
            console.log("Gamepad was not initialized!");
            return false;
        }

        event.preventDefault();

        if(ev.type == "touchstart" || ev.type == "mousedown" )
        {
            self.active = true;
            
            // all drag movements are instantaneous
            target.style.transition = '0s';

            // touch event fired before mouse event; prevent redundant mouse event from firing
            event.preventDefault();

            if (event.changedTouches)
            {
                // if this is a touch event, keep track of which one
                self.touchId = event.changedTouches[0].identifier;
                self.dragStart = { x: event.changedTouches[0].clientX, y: event.changedTouches[0].clientY };
            }
            else
            {
                self.dragStart = { x: event.clientX, y: event.clientY };
            }
        }

        if (self.active)
        {
            target.style.background = "";
            
            if(ev.type == "touchmove" || ev.type == "mousemove" )
            {
                target.style.background = "yellow";
                
                /**
                 * If this is a touch event, make sure it is the right one
                 * also handle multiple simultaneous touchmove events
                 **/
                var touchmoveId = null;
                if (event.changedTouches)
                {
                    for (var i = 0; i < event.changedTouches.length; i++)
                    {
                        if (self.touchId == event.changedTouches[i].identifier)
                        {
                            touchmoveId = i;
                            event.clientX = event.changedTouches[i].clientX;
                            event.clientY = event.changedTouches[i].clientY;
                        }
                    }

                    if (touchmoveId == null) return;
                }

                var xDiff = event.clientX - self.dragStart.x;
                var yDiff = event.clientY - self.dragStart.y;
                var angle = Math.atan2(yDiff, xDiff);
                var distance = Math.min(maxDistance, Math.hypot(xDiff, yDiff));

                var xPosition = distance * Math.cos(angle);
                var yPosition = distance * Math.sin(angle);

                //~ console.log(angle);

                // move stick image to new position
                target.style.transform = "translate("+xPosition+"px, "+yPosition+"px)";

                /**
                 * Deadzone adjustment
                 **/
                 
                distance = (distance < deadzone) ? 0 : maxDistance / (maxDistance - deadzone) * (distance - deadzone);
                xPosition = distance * Math.cos(angle);
                yPosition = distance * Math.sin(angle);
                
                self.value = { x: parseFloat((xPosition / maxDistance).toFixed(4)), y: parseFloat((yPosition / maxDistance).toFixed(4)) };

                //~ console.log(id +" "+ JSON.stringify(self.value));

                translateButton(angle);
            }
            
            if(ev.type == "touchend" || ev.type == "mouseup" )
            {
                // if this is a touch event, make sure it is the right one
                if (event.changedTouches && self.touchId != event.changedTouches[0].identifier) return;

                // transition the joystick position back to center
                target.style.transition = '.2s';
                target.style.transform = "translate(0px, 0px)";

                // reset everything
                self.value = { x: 0, y: 0 };
                self.touchId = null;
                self.active = false;

                resetGamepad();
            }

            //~ translateButton();
        }
    }

    target.addEventListener('mousedown', handle);
    document.addEventListener('mousemove', handle, {passive: false});
    document.addEventListener('mouseup', handle);

    target.addEventListener('touchstart', handle);
    document.addEventListener('touchmove', handle, {passive: false});
    document.addEventListener('touchend', handle);
}
