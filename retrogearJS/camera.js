/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 * 					camera.js - Scrolling management				  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *						Written by Cerullo Davide					  *
 *					   pix3lworkshop.altervista.org					  *
 *				   https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var Camera = function(surface, scrollBound=true, tilesize=16)
{
    var screen = document.getElementById("screen");
    
	this.surface = surface;
	this.scrollBound = scrollBound;

    this.CENTER_X = ((screen.clientWidth - tilesize) / 2)
    this.CENTER_Y = ((screen.clientHeight - tilesize) / 2)
        
    this.x = 0
    this.y = 0;
    
    //Max offsets
    this.w = curr_level.container.clientWidth - screen.clientWidth;
    this.h = curr_level.container.clientHeight - screen.clientHeight;

    this.init = function(x, y)
    {
        this.scrollCameraX(x);
        this.scrollCameraX(y);
    }
}

Camera.prototype.scrollCameraX = function(x)
{
    this.x += (x - this.x - this.CENTER_X);

    //Limit camera's movement inside the map width
    if(this.scrollBound)
    {
        if (this.x < 0)
        {
            this.x = 0;
        }
        
        if (this.surface.clientWidth - this.x <= screen.clientWidth)
        {
            this.x = this.surface.clientWidth - screen.clientWidth;
        }
    }

    this.surface.style.left = -this.x+"px";
}

Camera.prototype.scrollCameraY = function(y)
{
    this.y += (y - this.y - this.CENTER_Y);

    //Limit camera's movement inside the map width
    if(this.scrollBound)
    {
        if (this.y < 0)
        {
            this.y = 0;
        }
        
        if (this.surface.clientHeight - this.y <= screen.clientHeight)
        {
            this.y = this.surface.clientHeight - screen.clientHeight;
        }
    }
    
    this.surface.style.top = -this.y+"px";
}
