/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                   fps.js - Fps and system speed                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var FRAME_RATES = {
    FPS_RATE_30         : 33,  // 1000ms / 30fps = 33.33ms
    FPS_RATE_60         : 16,  // 1000ms / 60fps = 16.66ms
    FPS_RATE_120        : 8    // 1000ms / 120fps = 8.33ms
};

var frameCounter = {
    startTime: Date.now(),          // Time0
    currentTime: 0,                 // Time1
    rate: FRAME_RATES.FPS_RATE_30,  // Desired FPS rate
    frames: 0,                      // Displayed frames per seconds
    fpsTime: Date.now()             // Frame counter time start
};

function countFrames()
{
    // Check if a second has passed
    if (frameCounter.currentTime - frameCounter.fpsTime >= 1000)
    {
        var tmp = frameCounter.frames;
        
        frameCounter.frames = 0;
        frameCounter.fpsTime += 1000;
        
        return tmp;
    }

    return 0;
}
