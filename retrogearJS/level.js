/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                   level.js - Level handling function               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                  sourceforge.net/prolayerects/retrogear/           *
 *                                                                    *
 *********************************************************************/

var TILESIZE = 16;
var curr_level = null;

var MAP_LAYERS = {SOLID:0, BACKG:1, ALPHA:2, LAYERS: 3};

var Level = function(name, description, map, rows=10, cols=10, layers=3, theme) 
{
    this.name = name;
    this.description = description;
    this.theme = (theme === undefined)? "default" : theme;
    this.background_color = "";
    this.map = map;
    this.cols = cols;
    this.rows = rows;
    this.num_layers = layers;
    
    var curr_layer=1;

    this.container = document.getElementById("map-container");
    if(this.container==undefined)
    {
        this.container = document.createElement('div');
    }
    this.container.id = "map-container";
    //~ this.container.name = "map-"+name;
    this.container.style.position = "absolute";
    this.container.style.left = "0px";
    this.container.style.width = this.cols*TILESIZE+"px";
    this.container.style.height = this.rows*TILESIZE+"px";
    
    //~ this.assets = ["", "retrogearJS/resources/"+theme+"tile_sheet.png", "retrogearJS/resources/"+theme+"alpha_sheet.png"];
    this.assets = ["", "retrogearJS/resources/tile_sheet.png", "retrogearJS/resources/alpha_sheet.png"];

    //Preload assets
    for(var i=0; i < this.assets.length; i++)
    {
        (new Image()).src = this.assets[i];
    } 

    curr_level = this;
    
    this.init = function()
    {
    }

    this.getTile = function(layer, col, row)
    {
        if(this.map.indexOf(layer))
        {
            return this.map[layer][col + this.cols * row];
        }
        
        return 0;
    }
}

Level.prototype.drawTileMap = function()
{
    //Already drawed!
    if(this.container.hasChildNodes())
    {
        return;
    }
    
    var screen = document.getElementById("screen");
    if(!screen)
    {
        return;
    }
    
    var xPixOffset = 0;
    var yPixOffset = 0;
    var width = parseInt((screen.clientWidth / 16)+1);
    var height = parseInt((screen.clientHeight / 16)+1);

    for(var layer=1; layer<this.num_layers; layer++)
    {
        for(var row=0; row < this.rows; row++)
        {
            for(var col=0; col < this.cols; col++)
            {
                var tileIdx = this.getTile(layer, col, row);

                //~ console.log(tileIdx);
                
                if(parseInt(tileIdx)==0)
                {
                    continue;
                }
                
                xPixOffset = parseInt( ( Math.floor(tileIdx % 6)) * TILESIZE );
                yPixOffset = parseInt( ( Math.floor(tileIdx / 6)) * TILESIZE );

                var tile = document.createElement('div');
                tile.id=row+"-"+col;
                tile.setAttribute("class", "tile-"+tileIdx);
                tile.style.position = "absolute";
                tile.style.width = TILESIZE + "px";
                tile.style.height = TILESIZE + "px";
                //~ tile.style.float = "left";
                tile.style.zIndex = 0+layer;
                tile.style.background = "url('"+this.assets[layer]+"')";
                //~ console.log(assets[layer]);
                
                tile.style['left'] = col*TILESIZE + 'px';
                tile.style['top'] = row*TILESIZE + 'px';

                tile.style.backgroundRepeat = "no-repeat";
                tile.style.backgroundPosition = "-"+xPixOffset+"px -"+yPixOffset+"px";
                
                //~ console.log(tile);
                
                this.container.appendChild(tile);
            }
        }
    }

    screen.appendChild(this.container);
};
