/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                menu.js - Game menu handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var curr_menu = null;

var Menu_flags = {
    active         : 1 << 0,  // 000001  1
    show_title     : 1 << 1,  // 000010  2
    show_border    : 1 << 2,  // 000100  4
    persistent     : 1 << 3,  // 000100
    //~ flag     = 8     // 001000  8
};

var Menu = function(id, x=0, y=0, name, items, cursor='>')
{
    this.container = document.createElement('div');

    this.id = id;
    this.flags = 0;
    this.x = x;
    this.y = y;

    this.name = name;
    this.cursor = '>';
    this.items = items;

    this.curr_item = 0;
    this.page_start = 0;

    //Default container
    this.container.id = id;
    this.container.style.position = "absolute";
    this.container.style.left = this.x + 'px';
    this.container.style.top = this.y + 'px';
    //~ this.container.style.height = this.h + "px";
    this.container.style.zIndex = 10;
    //~ this.container.style.border = "1px solid black";

    this.cursor = font.renderText(this.id+"-cursor", ">");
    this.cursor.style.position = 'absolute';

    var width = 0;
    this.items = items;
    for(var i=0; i<this.items.length; i++)
    {
        this.items[i].x = this.x+16;
        this.items[i].y = i*8;
        width = (this.items[i].name.length > width)? this.items[i].name.length : width;

        this.items[i].container.style.position = 'absolute';
        this.items[i].container.style.left = 16 + 'px';
        this.items[i].container.style.top = this.items[i].y + 'px';

        //~ console.log(this.items[i]);
    }
    //~ this.container.style.width = (width*8+20) + "px";
    this.container.style.width =  "80px";

    //~ this.decorate();

    this.previous=null;
    this.next=null;

    //Use default callbacks
    this.update = this.update;
    this.draw = this.draw;
}

Menu.prototype.decorate = function()
{
    var width = parseInt(this.container.style.width);
    var height = this.container.style.height;
    
    var borderUp = document.createElement('div');
        borderUp.style.position = "absolute";
        borderUp.style.height = font.getCharHeight()+"px";
        borderUp.style.left = "-8px";
        borderUp.style.top = "-8px";

    var borderDown = borderUp;

    borderUp.appendChild(font.renderCharacter(String.fromCharCode(0)));
    
    while(width>16)
    {
        borderUp.appendChild(font.renderCharacter(String.fromCharCode(2)));
        width -= font.getCharWidth();
    }

    borderUp.appendChild(font.renderCharacter(String.fromCharCode(6)));

    borderDown.style.bottom = "8px";
    borderDown.style.bottom = "8px";

    this.container.innerHTML = borderUp.outerHTML+this.container.innerHTML;
    this.container.innerHTML = this.container.innerHTML+borderDown.outerHTML;

    console.log(width, borderUp);
}

Menu.prototype.getPrevious = function()
{
    if(this.previous!=null)
    {
        this.flags &= ~Menu_flags.active;
        curr_menu = this.previous;
    }
}

Menu.prototype.getNext = function()
{
    if(this.next!=null)
    {
        this.flags &= ~Menu_flags.active;
        curr_menu = this.next;
    }
}

Menu.prototype.update = function()
{
    if(!Gamepad.isButtonHeld(Gamepad.button.UP) && Gamepad.isButtonPressed(Gamepad.button.UP))
    {
        Gamepad.setButtonHeld(Gamepad.button.UP);
        if(curr_menu.curr_item > 0)
        {
            curr_menu.curr_item--;
        }
    }
    
    if(!Gamepad.isButtonHeld(Gamepad.button.DOWN) && Gamepad.isButtonPressed(Gamepad.button.DOWN))
    {
        Gamepad.setButtonHeld(Gamepad.button.DOWN);
        if(curr_menu.curr_item < curr_menu.items.length-1)
        {
            curr_menu.curr_item++;
        }
    }

    //Update cursor position
    var cursor = document.getElementById(curr_menu.cursor.id);
    cursor.style.top = curr_menu.items[curr_menu.curr_item].container.style.top;
    //~ console.log(cursor.id, cursor.style.top, this.items[this.curr_item].container.style.top, this.items[this.curr_item].name);

    if(Gamepad.isButtonPressed(Gamepad.button.A))
    {
        Gamepad.setButtonReleased(Gamepad.button.A);
        if(curr_menu.items[this.curr_item].callback!=null)
        {
            curr_menu.items[curr_menu.curr_item].callback();
        }
    }

    if(Gamepad.isButtonPressed(Gamepad.button.B))
    {
        Gamepad.setButtonReleased(Gamepad.button.B);
        if(curr_menu.previous != null)
        {
            curr_menu.flags &= ~Menu_flags.active;
            curr_menu.previous.flags |= Menu_flags.active;
            curr_menu.container.outerHTML = '';
            curr_menu.container.innerHTML = '';
            curr_menu.curr_item = 0;
            curr_menu = curr_menu.previous;
        }
    }

    //~ Gamepad.unsetDelay();
}

Menu.prototype.draw = function()
{
    var target =curr_menu.container;
    
    if(target.parentNode==null)
    {
        target = document.getElementById("screen").appendChild(curr_menu.container);
        //Draw the cursor one time
        target.appendChild(curr_menu.cursor);
    }
    
    if(curr_menu.items)
    {
        for(var i=0; i<curr_menu.items.length; i++)
        {
            if(!document.getElementById(curr_menu.items[i].container.id))
            {
                target.innerHTML += curr_menu.items[i].container.outerHTML;
                if(i<curr_menu.items.length-1)
                {
                    target.innerHTML +="<br>";
                }
                //~ font.drawText(items[i].id+"-text", 0, 0, items[i].name, items[i].container.id);

                //~ console.log(document.getElementById(items[i].id+"-text").parentNode);
            }

            //~ if(this.curr_item == i)
            //~ {
                //~ font.drawText(this.cursor.id+"-cursor", -this.x, items[i].y, ">", this.container.id);
            //~ }
        }
    }
}

var Item = function(id, name, value, callback, description="")
{
    this.id = "item-"+id;
    
    this.flags = 0;
    this.x = 0;
    this.y = 0;
    this.name = name;
    this.description = description;
    this.value = value;
    this.callback = callback;

    var label = font.renderText(this.id+"-text", this.name, this.x, this.y);

    this.container = document.createElement('div');
    this.container.id = id;
    this.container.style.position = "relative";
    this.container.style.float = 'left';
    this.container.style.width = parseInt((label.childNodes.length-1)*8)+"px";
    this.container.style.height = "8px";

    this.container.innerHTML = label.outerHTML;
}

Item.prototype.draw = function(parentId)
{

}
