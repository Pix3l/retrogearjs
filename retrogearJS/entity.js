/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                      entity.js - Entity management                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                  sourceforge.net/projects/retrogear/               *
 *                                                                    *
 *********************************************************************/

//Flag di status delle entity
var ENTITY_STATUS = {MOVE:0, ACTION:1, JUMP:2, FALL:3, CLIMB:4, IDLE:5, BLINK:6, KILL:7, DESTROY:8};

var Entity = function(id='', type=0, x=0, y=0, w=0, h=0, speed=0, gravity=0, update=null, sprite=null)
{
    this.container = document.createElement('div');

    this.next = null;

    this.type = type;       //Entity type
    this.flag_active = 1;
    
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.xstart = this.x;
    this.ystart = this.y;
    this.previous_x = this.x;
    this.previous_y = this.y;

    this.direction_x = 0;
    this.direction_y = 0;
    this.timer = [];    //Timer timer;

    this.speed = speed; // Its current speed (pixels per step).
    this.gravity = gravity; 
    this.hspeed = 0;
    this.vspeed = 0;

    this.status = ENTITY_STATUS.IDLE;
    this.update = update;       //Callback update function

    var offsetX = 0;
    var offsetY = 0;

    //~ if(curr_level!=undefined)
    //~ {
        //~ offsetX = parseInt(curr_level.container.style.left);
        //~ offsetY = parseInt(curr_level.container.style.top);
    //~ }

    //Default container
    this.container.id = id;
    this.container.style.position = "absolute";
    this.container.style.left = this.x + offsetX + 'px';
    this.container.style.top = (this.y + offsetY) + 'px';
    this.container.style.width = this.w + "px";
    this.container.style.height = this.h + "px";
    this.container.style.zIndex = 10;

    this.container.setAttribute("name", "entity-"+id);
    this.container.setAttribute("class", "entity");
    
    this.sprite = (typeof sprite === "undefined")? null : sprite;
    if(sprite)
    {
        this.sprite = sprite;
        //Sprite is positioned "absolute", set it at entity origin
        this.sprite.surface.container.style.left = "0px";
        this.sprite.surface.container.style.top = "0px";
        
        this.container.innerHTML = this.sprite.surface.container.outerHTML;
    }
}

Entity.prototype.setUpdate = function(callback)
{
    this.update = callback;
}

function handleCollision(entity, destination_x, destination_y)
{
    var point = {x: -1, y:-1};
    
    var col=0, origin_x=0;
    origin_x = pixelsToTiles( (entity.x < destination_x)? entity.x : destination_x );
    destination_x = pixelsToTiles( (entity.x > destination_x)? (entity.x + entity.w -1) : (destination_x + entity.w-1) );

    var row=0, origin_y=0;
    origin_y = pixelsToTiles( (entity.y < destination_y)? entity.y : destination_y );
    destination_y = pixelsToTiles( (entity.y > destination_y)? (entity.y + entity.h - 1) : (destination_y + entity.h - 1) );

    for (col = origin_x; col <= destination_x ; col++)
    {
        for (row = origin_y; row <= destination_y ; row++)
        {
            if(typeof curr_level !== 'undefined')
            {
                if (curr_level.map[MAP_LAYERS.SOLID][row][col]==TILE_TYPES.SOLID)
                {
                    point.x = tilesToPixels(col);
                    point.y = tilesToPixels(row);

                    //~ printf("[Entity] Collision detected at point %d,%d\n", point.x, point.y);
                    console.log("[Entity] Collision detected at point "+point.x+","+point.y);
                    
                    break;
                }
            }
        }
    }

    return point;
}

Entity.prototype.moveX = function(x)
{
    var offsetX = (typeof curr_level !== 'undefined')? curr_level.container.style.left : 0;
    var camera_x = (typeof camera !== 'undefined')? camera.x : 0;
    
    this.x = x;

    if(parseInt(this.container.style.left) != x)
    {
        this.container.style.left = (this.x - camera_x) + 'px';
    }
}

Entity.prototype.moveY = function(y)
{
    var offsetY = (typeof curr_level !== 'undefined')? curr_level.container.style.top : 0;
    var camera_y = (typeof camera !== 'undefined')? camera.y : 0;
    
    this.y = y;

    if(parseInt(this.container.style.top) != y)
    {
        this.container.style.top = (this.y - camera_y) + 'px';
    }
    
}

/**
 * Handles the gravity and jumps for entities
 **/
Entity.prototype.doGravity = function()
{
    var point = new RG_Point(0, -1);
    
    //Checking collision in the up direction
    if(this.vspeed<0)
    {
        //Has collide on top?
        point = getTileCollision(this, this.x, Math.floor(this.y+this.vspeed));
        if( point.y != -1 )
        {
            this.y = point.y + TILESIZE;   //Place the object nearest the top tile
            this.vspeed = 0;

            this.direction_y = 0;
        }
        else
        {
            this.direction_y = -1;
            this.status = ENTITY_STATUS.JUMP;
            this.y += this.vspeed;         //Update its position
            this.vspeed += this.gravity;   //Gravity continues to push down
        }
    }
    else
    {
        //Has collide on bottom?
        point = getTileCollision(this, this.x, Math.ceil(this.y+this.vspeed));
        //~ console.log(point);
        if( point.y != -1 )
        {
            this.y = Math.ceil(point.y - this.h);   //Place the object on top of the bottom tile
            this.vspeed = 1;   //1 so we test against the ground again int the next frame (0 would test against the ground in the next+1 frame)

            if(this.status!=ENTITY_STATUS.MOVE)
            {
                this.status = ENTITY_STATUS.IDLE;
                this.direction_y = 0;
            }

            this.direction_y = 0;
        }
        else
        {
            this.direction_y = 1;
            this.status = ENTITY_STATUS.FALL;
            this.y += this.vspeed;         //Update its position
            this.vspeed += this.gravity;   //Gravity continues to push down

            console.log("Fall");

            if( this.vspeed >= TILESIZE)    
                this.vspeed = TILESIZE;
        }
    }
}

Entity.prototype.isOnFloor = function()
{
    if(this.vspeed < 0)
        return 0;
    
    var leftTile = (curr_level.map[MAP_LAYERS.SOLID][ (pixelsToTiles(this.y+this.h+1) * curr_level.cols) + pixelsToTiles(this.x+1)]==TILE_TYPES.SOLID);
    var rightTile = (curr_level.map[MAP_LAYERS.SOLID][ (pixelsToTiles(this.y+this.h+1) * curr_level.cols) + pixelsToTiles(this.x+this.w-1)]==TILE_TYPES.SOLID);

    var onGround = (leftTile || rightTile)? 1 : 0;

    return onGround;
}

Entity.prototype.addTimer = function(interval, callback, params=null, repeat=false)
{
    if (typeof Timer !== 'undefined')
    {
        this.timer.push(new Timer(interval, callback, params, repeat));
    }
}

Entity.prototype.destroy = function()
{
    var current = Entities.head;
    var previous = null;
    while (current!=this)
    {
        previous = current;
        current = current.next;
    }
    
    var target = document.getElementById(this.container.id);
    if(target!=undefined && target.parentNode)
    {
        target.parentNode.removeChild(target);
        this.sprite = null;
    }
    
    previous.next = current.next;
}

Entity.prototype.draw = function()
{
    //Entity can be still referenced somewhere after pop, prevent drawing
    if(this.status==ENTITY_STATUS.DESTROY)
    {
        return;
    }
    
    if(this.container.parentNode==null)
    {
        document.getElementById("screen").appendChild(this.container);
    }

    if(this.sprite!=null)
    {
        //Rewrite the inner content only if the sprite is changed
        if(this.container.children[0]!=undefined && (this.container.children[0].style.backgroundPosition != this.sprite.surface.container.style.backgroundPosition) )
        {
            this.container.innerHTML = this.sprite.surface.container.outerHTML;
        }
    }

    //~ this.sprite.animate();
}

//Generic entity linked list
var EntityList = function()
{
    this.head = null;
    this.tail = null;
}

EntityList.prototype.print = function()
{
    var current = this.head;
    while(current.next!=null)
    {
        console.log(current);
        current = current.next;
    }
}

EntityList.prototype.exist = function(id)
{
    return document.getElementById(id)!==null;
}

EntityList.prototype.push = function(entity)
{
    var current = this.head;
    if (current == null)
    {
        this.head = entity;
    }
    else
    {
        while(current.next!=null)
        {
            current = current.next;
        }

        current.next = entity;
        this.tail = entity;
    }
}

EntityList.prototype.pop = function(entity)
{
    var current = this.head;
    var previous = current;
    while(current!=null)
    {
        if(current==entity)
        {
            break;
        }
        
        previous = current;
        current = current.next;
    }

    var target = document.getElementById(current.container.id);
    if(target!==undefined)
    {
        var remove = target.parentNode.removeChild(target);
    }

    current.status = ENTITY_STATUS.DESTROY;
    
    previous.next = current.next;
    
    if(current==this.head)
    {
        this.head = current.next;
    }
    
    if(current==this.tail)
    {
        this.tail = previous;
        previous.next = null;
    }
}
