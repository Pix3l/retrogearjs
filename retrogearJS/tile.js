/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                      tile.js - Tiles related functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

//Reserved values
var TILE_TYPES = {SOLID:1, WARP:3};

/**
 * Controlla se una coordinata rientra o meno in un possibile tile del
 * livello in base alla grandezza di esso definita globalmente.
 * 
 * Utile per controllare l'allineamento di una Entity su un'ipotetica
 * griglia di gioco
 * 
 * @param int x, int y
 *      Coordinate da controllare sul campo di gioco
 * 
 * @return int
 *      0 Se non e' allineato con i tile
 *      1 Se e' allineato con i tile
 **/
function isInTile(x, y)
{
    var cols, rows;
    // Se non c'e' resto siamo all'interno del tile
    cols = !(x%TILESIZE); //Asse x
    rows = !(y%TILESIZE); //Asse y
    // Ritorniamo 1 se siamo nel tile
    if(cols==1 && rows==1)
     return 1;

    return 0; // Non e' perfettamente nel tile
}

/**
 * Converts a pixel position to a tile position.
 */
function pixelsToTiles(pixels)
{
    return parseInt(Math.floor(pixels/TILESIZE));
}

/**
 * Converts a tile position to a pixel position.
 */
function tilesToPixels(numTiles)
{
    // use this if the tile size isn't a power of 2:
    return numTiles * TILESIZE;
}

function getTileCollision(entity, new_x, new_y)
{
    var col=0, row=0,
        minx=0, miny=0,
        maxx=0, maxy=0;

    var point = new RG_Point(-1, -1);
    
    // From pixels to tiles
    minx = parseInt(pixelsToTiles( (entity.x < new_x)? entity.x : new_x ));
    miny = parseInt(pixelsToTiles( (entity.y < new_y)? entity.y : new_y ));

    //Left : Right
    maxx = parseInt(pixelsToTiles( (entity.x > new_x)? (entity.x + entity.w -1) : (new_x + entity.w-1) ));
    //Up : Down
    maxy = parseInt(pixelsToTiles( (entity.y > new_y)? (entity.y + entity.h - 1) : (new_y + entity.h - 1) ));

    // We return the coordinates of the tile with which the entity collides
    for (row = miny ; row <= maxy ; row++)
    {
        for (col = minx; col <= maxx ; col++)
        {
            if (curr_level.getTile(0, col, row)==1)
            {
                point.x = tilesToPixels(col);
                point.y = tilesToPixels(row);
                break;
            }
        }
    }

    return point;
}
