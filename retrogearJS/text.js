var Text = function(id, x, y, text)
{
    this.text = "";
    
    this.container = document.createElement('div');
    this.container.id = id;
    this.container.style.position = "absolute";

    this.font = _defaultFont;

    this.setPosition(x,y);
    this.set(text);
}

Text.prototype.setPosition = function(x, y)
{
    this.container.style.left = x + 'px';
    this.container.style.top  = y + 'px';
}

Text.prototype.set = function(text)
{
    if(text!==undefined && this.text===text)
    {
        //~ console.log("[Text] The new text is the same as the last");
        return;
    }

    this.text = text;
    this.container.innerHTML = "";  //Reset
    
    //Convert every character to an image equivalent
    var xPixOffset, yPixOffset;
    for (var i=0; i < text.length; i++)
    {
        if(text[i]=="\n")
        {
            var nl = document.createElement('div');
            //~ nl.style.clear = "both";
            nl.style.position = "relative";
            nl.style.height = this.font.getCharHeight()+"px";
            this.container.appendChild(nl);
            continue;
        }

        xPixOffset = parseInt( ( Math.floor(text.charCodeAt(i) % 32)) * this.font.char_w );
        yPixOffset = parseInt( ( Math.floor(text.charCodeAt(i) / 32)) * this.font.char_h );

        var character = document.createElement('div');
        character.style.position = "relative";
        character.style.width = this.font.getCharWidth() + "px";
        character.style.height = this.font.getCharHeight() + "px";
        character.style.float = "left";

        character.style.background = "url('"+this.font.file+"')";
        character.style.backgroundRepeat = "no-repeat";
        character.style.backgroundPosition = "-"+xPixOffset+"px -"+yPixOffset+"px";

        this.container.appendChild(character);
    }
}

Text.prototype.draw = function(text)
{
    //Text not set or not changed, skip...
    if(text!==undefined && this.text===text)
    {
        //~ console.log("[Text] The new text is the same as the last");
        return;
    }
    
    //Item already exists in DOM, skip...
    if(this.container.parentNode!==null)
    {
        return;
    }

    document.getElementById("screen").appendChild(this.container);
}