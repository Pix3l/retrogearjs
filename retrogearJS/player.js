/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 * 						player.js - player handling					  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *						Written by Cerullo Davide					  *
 *					   pix3lworkshop.altervista.org					  *
 *				   https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

var curr_player;	//Current player active object

var sprite = new Sprite(16, 16, 16, 16, 1, "assets/player.png");
var player1 = new Entity('player1', 0, 288, 224, 16, 16, 1, 0, updatePlayer, sprite);
//~ entity.speed = 1;
player1.direction_x=1;
player1.direction_y=1;

//~ console.log(player1);

/**
 * Reset player's default values
 * 
 * @param Object player
 * 		Pointer to a player object
 **/
function initplayer(player)
{
    //~ player = new Entity('player', 'PLAYER', 208, 416, 26, 16, 3, "player.png", 0.5, 1, 0, moveplayer);
}

function updatePlayer()
{
    camera.scrollCameraX(player1.x);
    camera.scrollCameraY(player1.y);

    if(Gamepad.isButtonPressed(Gamepad.button.LEFT))
    {
        player1.moveX(player1.x-player1.speed);
    }

    if(Gamepad.isButtonPressed(Gamepad.button.RIGHT))
    {
        player1.moveX(player1.x+player1.speed);
    }
    
    if(Gamepad.isButtonPressed(Gamepad.button.UP))
    {
        player1.moveY(player1.y-player1.speed);
    }
    
    if(Gamepad.isButtonPressed(Gamepad.button.DOWN))
    {
        player1.moveY(player1.y+player1.speed);
    }
    
    //~ console.log(player1);
    
    //~ player1.sprite.animate();
}
