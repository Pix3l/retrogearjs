/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 * 					 retrogear.js - Main engine source				  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *						Written by Cerullo Davide					  *
 *					   pix3lworkshop.altervista.org					  *
 *				   https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/ 

function rectCollision(x1, y1, w1, h1, x2, y2, w2, h2)
{
    if (y1+h1 < y2) return 0;
    if (y1 > y2+h2) return 0;
    if (x1+w1 < x2) return 0;
    if (x1 > x2+w2) return 0;
    
    return 1;
}

/**
 * User defined variables initialization
 **/
function mainInit()
{
}

function init()
{
    Game.status = states.TITLE;
    initFont();

    curr_player = player1;

    mainInit();
    mainLoop();
}
