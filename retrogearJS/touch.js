/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 * 						contols.js - Input handling					  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *						Written by Cerullo Davide					  *
 *					   pix3lworkshop.altervista.org					  *
 *				   https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/
 
function handleTouch(ev)
{
    if (typeof Gamepad == "undefined")
    {
        console.log("Gamepad was not initialized!");
        return false;
    }

    var key = ev.target.id.replace("btn", "");
    var keyCode = Gamepad.button[key];

    //NOTE: This doesn't work well with css :active selector
    //~ ev.preventDefault();

    for(var i=0; i<Gamepad.buttons.length; i++)
    {
        if( Gamepad.buttons[i] == keyCode )
        {
            if(ev.type == "touchstart" || ev.type == "mousedown" )
            {
                console.log(ev.type);
                Gamepad.setButtonPressed(keyCode);
                continue;
            }
            
            if(ev.type == "touchend" || ev.type == "mouseup")
            {
                console.log(ev.type);
                Gamepad.setButtonReleased(keyCode);

                console.log("release");
                continue;
            }
        }
    }

    //~ console.log(Gamepad.state);
}

window.addEventListener('load', function(){

    /**
     * This is an experimental technology
     * https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/ontouchstart
     **/
    document.ontouchstart = function(event){handleTouch(event);}; 
    document.ontouchend = function(event){handleTouch(event);};
    //~ document.onclick = function(event){handleTouch(event);};
    document.onmousedown = function(event){handleTouch(event);};
    document.onmouseup = function(event){handleTouch(event);};
    //~ document.oncontextmenu = function(event){event.preventDefault(); handleTouch(event);};
});
