/**********************************************************************
 *                                                                    *
 *                            RetroGearJS                             *
 *                                                                    *
 *         Javascript generic 2D game engine based on RetroGear       *
 *                      sprite.js - Sprite library                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                 https://gitlab.com/Pix3l/retrogearjs               *
 *                                                                    *
 *********************************************************************/

//animation_speed=-1 - Nessuna animazione (Oggetto statico)
var Sprite = function(x, y, w, h, animation_speed, file)
{
    (new Image()).src = file;    //Preload
    
    this.max_frames = 6;     //Max frames per line

    this.w = w;
    this.h = h;

    this.index = 0;
    this.animation_timer = 0;
    this.animation_speed = animation_speed;
    this.animation_sequence = null;

    this.surface = new Surface("id-" + Math.random().toString(16).slice(2), x, y, w, h, file);
    this.surface.container.setAttribute("name", "sprite-"+this.surface.container.id);
    this.surface.container.setAttribute("class", "sprite sprite-"+this.surface.container.id);
}

Sprite.prototype.setSprite = function(entity, x, y, w, h, animation_speed, file)
{
    //~ var _sprite = document.getElementById(entity.sprite.surface.id);
    
    entity.sprite.surface.style.position = "absolute";
    entity.sprite.surface.style.left = x + 'px';
    entity.sprite.surface.style.top  = y + 'px';
    
    entity.sprite.surface.style.background = "url('"+sprite+"')";
    entity.sprite.surface.style.backgroundRepeat = "no-repeat";
    entity.sprite.surface.style.backgroundPosition = "0 0";
    
    console.log(this.sprite.surface);
    
    if(entity.sprite.surface.parentNode==null)
    {
        document.getElementById("screen").appendChild(this.sprite.surface);
    }
}

Sprite.prototype.setAnimationSequence = function(sequence)
{
    this.animation_sequence = sequence;
    //~ this.index = 0;
}

Sprite.prototype.setFrame = function(index)
{
    var xPixOffset = parseInt( ( Math.floor(index % this.max_frames)) * this.w );
    var yPixOffset = parseInt( ( Math.floor(index / this.max_frames)) * this.h );
    //~ var yPixOffset = 0;

    //~ console.log("[Sprite] setFrame: "+xPixOffset, yPixOffset);

    this.surface.container.style.backgroundPosition = "-"+xPixOffset+"px -"+yPixOffset+"px";

    //~ console.log("[Sprite] "+this.surface.container.id+"::backgroundPosition: "+this.surface.container.style.backgroundPosition);
};

Sprite.prototype.getFrame = function(index)
{
    var _surface = this.surface.cloneNode();
    _surface.style.backgroundPositionX = index+'px';

    return _surface;
};

/**
 * Loop the animation
 **/
Sprite.prototype.animate = function()
{
    if(this.animation_sequence==null)
    {
        return;
    }

    //If the frame index is not set or the sequence is changed, reset to the first char
    if(this.index==null)
    {
        this.index = this.animation_sequence;
    }

    var curr_time = +new Date;
    if( (curr_time - Math.abs(this.animation_speed)) >= this.animation_timer)
    {
        this.animation_timer=curr_time;
        this.index++;
    }

    //End of the sequence, reset
    if(this.index==-1 || this.index >= this.animation_sequence.length)
    {
        this.index = 0;
    }

    var _frame = (this.animation_sequence!=null &&this.animation_sequence[this.index]!== undefined)?
                    this.animation_sequence[this.index] : this.index;
    this.setFrame(_frame);
};

Sprite.prototype.hasAnimationEnded = function()
{
    //~ console.log(this.animation_timer, this.index);
    if(this.animation_timer > 0 && this.index==0)
    {
        return true;
    }
    
    return false;
};

Sprite.prototype.draw = function()
{
    return this.surface.draw();
    //If sprite already exists in DOM and has the same coordinates, we skip it
    //~ if(this.surface.container.parentNode==null)
    //~ {
        //~ document.getElementById("screen").appendChild(this.surface);
        //~ return true;
    //~ }

    //~ return false;
};

Sprite.prototype.drawFrame = function(x, y, frame)
{
    if(this.draw())
    {
        this.surface.style.backgroundPositionX = "-"+frame+'px';
        this.surface.style.left = x + 'px';
        this.surface.style.top  = parseInt(y) + 'px';
        this.index = frame/this.w;
    }
};
